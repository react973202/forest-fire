import type { AppProps } from "next/app";
import Head from "next/head";
import "../styles/globals.css";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Forest fire</title>
        <link rel="shortcut icon" href="favicon.png" />
      </Head>

      <>
        <Component {...pageProps} />
      </>
    </div>
  );
}
