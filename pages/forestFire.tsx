import { NextPage } from "next";
import ForestFireContainer from "../components/forestFire/forestFireContainer";

const ForestFirePage: NextPage = () => {
  return <ForestFireContainer />;
};

export default ForestFirePage;
