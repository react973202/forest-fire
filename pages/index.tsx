import { Container, Grid, Typography } from "@mui/material";
import { NextPage } from "next";
import Image from "next/image";
import D5 from "../public/D5.png";
import D6 from "../public/D6.png";
import D7 from "../public/D7.png";
import D8 from "../public/D8.png";
import { useRouter } from "next/router";

const HomePage: NextPage = () => {
  const router = useRouter();

  const handlemodel = (numberCase: number) => {
    router.push(`/forestFire?q=${numberCase}`);
  };

  return (
    <Container>
      <Typography variant="h1" component="h2" color="green">
        Choisissez un model
      </Typography>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid
          item
          xs={6}
          className="overlay-image"
          onClick={() => handlemodel(5)}
        >
          <Image
            src={D5}
            alt=""
            width={500}
            height={500}
            className="image-page-create-article"
          />
          <Typography className="hover-image" variant="h1" component="h2">
            {"5x5"}
          </Typography>
        </Grid>

        <Grid
          item
          xs={6}
          className="overlay-image"
          onClick={() => handlemodel(6)}
        >
          <Image src={D6} alt="" width={500} height={500} />
          <Typography className="hover-image" variant="h1" component="h2">
            {"6x6"}
          </Typography>
        </Grid>

        <Grid
          item
          xs={6}
          className="overlay-image"
          onClick={() => handlemodel(7)}
        >
          <Image
            src={D7}
            alt=""
            width={500}
            height={500}
            className="image-page-create-article"
          />
          <Typography className="hover-image" variant="h1" component="h2">
            {"7x7"}
          </Typography>
        </Grid>

        <Grid
          item
          xs={6}
          className="overlay-image"
          onClick={() => handlemodel(8)}
        >
          <Image
            src={D8}
            alt=""
            width={500}
            height={500}
            className="image-page-create-article"
          />
          <Typography className="hover-image" variant="h1" component="h2">
            {"8x8"}
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
};

export default HomePage;
