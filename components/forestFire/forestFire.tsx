import * as React from "react";
import { Button, Container } from "@mui/material";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

const ForestFire = () => {
  const router = useRouter();
  const { q } = router.query;

  const [numberRows, setNumberRows] = useState<any>(q);
  const [disabledCells, setDisabledCells] = useState<any>([]);

  const [currentFire, setCurrentFire] = useState<{
    row: number;
    column: number;
  }>({ row: -1, column: -1 });

  useEffect(() => {
    setNumberRows(q);
  }, [q, router.pathname]);

  //Initialiser une cellule
  useEffect(() => {
    const value1 = Math.floor(Math.random() * numberRows);
    const value2 = Math.floor(Math.random() * numberRows);
    setCurrentFire({ row: value1, column: value2 });
  }, []);

  const handleCellClick = (rowIndex: number, columnIndex: number) => {
    // Ajouter l'ancienne cellule aux cellules désactivées
    if (currentFire.row !== -1 && currentFire.column !== -1) {
      setDisabledCells([
        ...disabledCells,
        `${currentFire.row}-${currentFire.column}`,
      ]);
    }
    // Mettre à jour la nouvelle cellule en feu
    setCurrentFire({ row: rowIndex, column: columnIndex });
  };

  // Recommencer dés le debut
  const handleRefresh = () => {
    setDisabledCells([]);
    const value1 = Math.floor(Math.random() * numberRows);
    const value2 = Math.floor(Math.random() * numberRows);
    setCurrentFire({ row: value1, column: value2 });
  };

  return (
    <Container maxWidth="lg">
      <TableContainer component={Paper}>
        <Table style={{ borderCollapse: "separate", borderSpacing: "2px" }}>
          <TableBody>
            {Array.from({ length: numberRows }).map(
              (row: any, rowIndex: any) => (
                <TableRow key={rowIndex}>
                  {Array.from({ length: numberRows }).map(
                    (column: any, columnIndex: any) => {
                      const cellKey = `${rowIndex}-${columnIndex}`;
                      const isDisabled = disabledCells.includes(
                        cellKey as never
                      );
                      const isCurrentFire =
                        currentFire.row === rowIndex &&
                        currentFire.column === columnIndex;
                      const column1 = `${currentFire.row - 1}-${
                        currentFire.column
                      }`;
                      const column2 = `${currentFire.row + 1}-${
                        currentFire.column
                      }`;
                      const row1 = `${currentFire.row}-${
                        currentFire.column - 1
                      }`;
                      const row2 = `${currentFire.row}-${
                        currentFire.column + 1
                      }`;

                      return (
                        <TableCell
                          key={columnIndex}
                          align="center"
                          onClick={() =>
                            (column1 == cellKey ||
                              column2 == cellKey ||
                              row1 == cellKey ||
                              row2 == cellKey) &&
                            handleCellClick(rowIndex, columnIndex)
                          }
                          style={{
                            height: "7rem",
                            // minWidth: "15rem",
                            backgroundColor: isDisabled
                              ? "gray"
                              : isCurrentFire
                              ? "#E11000"
                              : column1 == cellKey ||
                                column2 == cellKey ||
                                row1 == cellKey ||
                                row2 == cellKey
                              ? "#E08D00"
                              : "#00850B",
                            borderWidth: "2px",
                            borderStyle: "solid",
                            cursor: "pointer",
                          }}
                        ></TableCell>
                      );
                    }
                  )}
                </TableRow>
              )
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Button variant="contained" color="success" onClick={handleRefresh}>
        Recommencer
      </Button>
    </Container>
  );
};

export default ForestFire;
